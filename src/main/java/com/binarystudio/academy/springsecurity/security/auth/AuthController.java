package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.security.auth.model.*;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("auth")
public class AuthController {
    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("me")
    public User whoAmI(@AuthenticationPrincipal User user) {
        return user;
    }

    @PostMapping("safe/login")
    public AuthorizationResponse login(@RequestBody AuthorizationRequest authorizationRequest) {
        return authService.performLogin(authorizationRequest);
    }

    @PostMapping("safe/register")
    public AuthorizationResponse register(@RequestBody RegistrationRequest registrationRequest) {
        return authService.performRegistration(registrationRequest);
    }

    @PostMapping("safe/refresh")
    public AuthorizationResponse refreshTokenPair(@RequestBody RefreshTokenRequest refreshTokenRequest) {
        return authService.performRefreshTokenPair(refreshTokenRequest);
    }

    @PutMapping("safe/forgotten_password")
    public void forgotPasswordRequest(@RequestParam String email) {
        authService.performEmailConfirmation(email);
    }

    @PatchMapping("safe/forgotten_password")
    public AuthorizationResponse forgottenPasswordReplacement(@RequestBody ForgottenPasswordReplacementRequest forgottenPasswordReplacementRequest) {
        return authService.performPasswordReplacement(forgottenPasswordReplacementRequest);
    }

    @PatchMapping("change_password")
    public AuthorizationResponse changePassword(@AuthenticationPrincipal User user, @RequestBody PasswordChangeRequest passwordChangeRequest) {
        return authService.performChangingPassword(user, passwordChangeRequest);
    }
}
