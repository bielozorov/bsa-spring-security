package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.UserService;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.security.auth.model.*;
import com.binarystudio.academy.springsecurity.security.jwt.JwtProvider;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@Log4j2
public class AuthService {
    private final UserService userService;
    private final JwtProvider jwtProvider;
    private final PasswordEncoder passwordEncoder;

    public AuthService(UserService userService, JwtProvider jwtProvider, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.jwtProvider = jwtProvider;
        this.passwordEncoder = passwordEncoder;
    }

    public AuthorizationResponse performLogin(AuthorizationRequest authorizationRequest) {
        User userDetails = userService.loadUserByUsername(authorizationRequest.getUsername());
        if (passwordsDontMatch(authorizationRequest.getPassword(), userDetails.getPassword())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
        }
        return AuthorizationResponse.of(jwtProvider.generateToken(userDetails), jwtProvider.generateRefreshToken(userDetails));
    }

    private boolean passwordsDontMatch(String rawPw, String encodedPw) {
        return !passwordEncoder.matches(rawPw, encodedPw);
    }

    public AuthorizationResponse performRegistration(RegistrationRequest registrationRequest) {
        User userDetails = userService.createUser(registrationRequest.getLogin(), registrationRequest.getEmail(), passwordEncoder.encode(registrationRequest.getPassword()));
        return AuthorizationResponse.of(jwtProvider.generateToken(userDetails), jwtProvider.generateRefreshToken(userDetails));
    }

    public AuthorizationResponse performChangingPassword(User user, PasswordChangeRequest passwordChangeRequest) {
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User unauthorized");
        }
        User userDetails = userService.updatePassword(user, passwordEncoder.encode(passwordChangeRequest.getNewPassword()));
        return AuthorizationResponse.of(jwtProvider.generateToken(userDetails), jwtProvider.generateRefreshToken(userDetails));
    }

    public void performEmailConfirmation(String email) {
        User userDetails = userService.getByEmail(email);
        log.info(jwtProvider.generateToken(userDetails));
    }

    public AuthorizationResponse performRefreshTokenPair(RefreshTokenRequest refreshTokenRequest) {
        String login = jwtProvider.getLoginFromToken(refreshTokenRequest.getRefreshToken());
        User userDetails = userService.loadUserByUsername(login);
        return AuthorizationResponse.of(jwtProvider.generateToken(userDetails), jwtProvider.generateRefreshToken(userDetails));
    }

    public AuthorizationResponse performPasswordReplacement(ForgottenPasswordReplacementRequest forgottenPasswordReplacementRequest) {
        String login = jwtProvider.getLoginFromToken(forgottenPasswordReplacementRequest.getToken());
        User newUserDetails = userService.updatePassword(userService.loadUserByUsername(login), forgottenPasswordReplacementRequest.getNewPassword());
        return AuthorizationResponse.of(jwtProvider.generateToken(newUserDetails), jwtProvider.generateRefreshToken(newUserDetails));
    }

}
