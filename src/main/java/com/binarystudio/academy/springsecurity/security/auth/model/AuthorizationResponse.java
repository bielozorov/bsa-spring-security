package com.binarystudio.academy.springsecurity.security.auth.model;

import lombok.Data;

@Data
public class AuthorizationResponse {
    private String accessToken;
    private String refreshToken;

    public static AuthorizationResponse of(String accessToken, String refreshToken) {
        AuthorizationResponse response = new AuthorizationResponse();
        response.setAccessToken(accessToken);
        response.setRefreshToken(refreshToken);
        return response;
    }
}
